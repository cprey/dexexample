# DEX tutorial with GitLab, minikube, and a local dex client for OSX

> PURPOSE - this will create a sample client that you can authenticate to using Dex and GitLab. Nothing here is meant to suggest a best practice in general, don't put secrets directly into a configuration file. Don't share your application secrets from GitLab. There is no SSL whatsoever on this - you have been warned.

The goal is to lower the cognative load to help you understand the relationships between the values.yaml file, the GitLab application configuration, and the golang sample client application. Smart people struggle over this so take your time.

The steps to get this working:

1. start minikube and use helm to deploy Dex using the supplied `values.yaml` file
    * add the helm chart - `helm repo add dex https://charts.dexidp.io`
    * install the helm chart `helm install dexterhelmsly dex/dex -f values.yaml` _extra points for picking an amusing name_ I chose *dexterhelmsly*
    * use kubectl to watch the pod and service come up and become available.

    ```shell
        example-app git:(master) ✗ kubectl get all
        NAME                                 READY   STATUS    RESTARTS   AGE
        pod/dexterhelmsly-784bf66f96-nnjcd   1/1     Running   0          5h18m

        NAME                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)             AGE
        service/dexterhelmsly   ClusterIP   10.102.60.130   <none>        5556/TCP,5558/TCP   21h
        service/kubernetes      ClusterIP   10.96.0.1       <none>        443/TCP             47h

        NAME                            READY   UP-TO-DATE   AVAILABLE   AGE
        deployment.apps/dexterhelmsly   1/1     1            1           21h

        NAME                                       DESIRED   CURRENT   READY   AGE
        replicaset.apps/dexterhelmsly-784bf66f96   1         1         1       5h18m
    ```

1. create the mapping to the minikube service - mine above is *service/dexterhelmsly*
    * `minikube service --url dexterhelmsly` will spit out a chart with the mappings.

    ```shell
        😿  service default/dexterhelmsly has no node port
        🏃  Starting tunnel for service dexterhelmsly.
        |-----------|---------------|-------------|------------------------|
        | NAMESPACE |     NAME      | TARGET PORT |          URL           |
        |-----------|---------------|-------------|------------------------|
        | default   | dexterhelmsly |             | http://127.0.0.1:63631 |
        |           |               |             | http://127.0.0.1:63632 |
        |-----------|---------------|-------------|------------------------|
    ```

    * the two kubernetes ports for _service/dexterhelmsly_ 5556 and 5558 are mapped to http://127.0.0.1:63631 and http://127.0.0.1:63632 respectively.
    * the first value is mapped to Dex _web_ which is defined on line 9 of `values.yaml` listening on all interfaces, port 5556.
    * update the ports on line 3 and line 29 to match the first port you got back when you ran `minikube service --url dexterhelmsly`

1. create an /etc/hosts entry for `my-issuer-url.com` at 127.0.0.1

1. This tutorial uses the free tier of GitLab as OAuth provider.
    * create an `application`by clicking on your avatar and select `Preferences --> Applications`
    * fill out the *Add new application* form using the following values - swap out the port 63631 with the port value from `minikube service --url dexterhelmsly`
        * Name: Example App
        * Redirect URI: http://my-issuer-url.com:63631/dex/callback
        * Confidential: Checked
        * Scopes:
            * read_user
            * openid
    * Save the form which should look something like this.

        ![GitLab Application Configuration](/images/Gitlabconfiguration.png)
    * An *Application ID* and *Secret* have been generated
        * Copy the Application ID from the GitLab form and replace the `values.yaml` *ClientID* field on line 27.
        * Copy the Secret from the GitLab form and replace the `values.yaml` *clientSecret* field on line 28.
        * Ensure the Callback URL in the GitLab form matches the `values.yaml` *redirectURI* field on line 29.

1. build golang example app
    * `cd dex/examples/example-app; go build`
    * This will make an executable in the current working directory called *example-app*, you _can't_ call it using `go run main.go`

1. start the golang example app with the following flags.
    * you will need to edit the port 63631 to match the port you got from above
    * `./example-app --issuer http://my-issuer-url.com:63631/dex  --redirect-uri http://localhost:5555/callback --debug`

1. The last step is to update the GitLab application with the correct Callback URL port. There is a chicken and egg problem here. Update the port to the port value from `minikube service --url dexterhelmsly` and save your changes.

1. We're finally ready to to see if it's working
    * open a browser and hit <http://localhost:5555>

    ![localhost1](/images/localhost1.png)
    * hit the Login button - you should now see

    ![localhost2](/images/localhost2.png)
    * choose Log in with GitLab

    ![localhost3](/images/localhost3.png)
    * Click Grant access - if you see token information *Congratulations* you did it!

1. Try the other examples - hook your system up to LDAP or GitHub. Create additional apps or use Dex to authenticate to your minikube instance.

## Troubleshooting

* ensure the example-app flags match what's in the values.yaml file and GitLab Application.
  * running `example-app` with no flags will show configuration options

* get the logs from the pod
  * use `kubectl get all` and copy your pod value for dexterhelmsly
  * for my deployment I would use `kubectl logs -f pod/dexterhelmsly-784bf66f96-nnjcd`
  * ctrl+c to stop
  * `helm uninstall dexterhelmsly` cleans up everything

* If you would like to make changes to `values.yaml` you can redeploy them using helm. Since your service doesn't change you won't have to rerun `minikube service`

  ```shell
  helm upgrade dexterhelmsly dex/dex -f values.yaml
  ```
